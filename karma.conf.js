const path = require('path');
const webpack = require('./webpack.config');
delete webpack.entry;

module.exports = function karmaConfig(config) {
  config.set({
    frameworks: [ 'mocha', 'chai', 'es6-shim' ],
    reporters: [ 'progress', 'coverage-istanbul' ],
    files: [
      'node_modules/babel-polyfill/dist/polyfill.js',
      'node_modules/whatwg-fetch/fetch.js',
      'src/**/*spec.js'
    ],
    preprocessors: {
      'src/**/*spec.js': [ 'webpack', 'sourcemap' ]
    },
    browsers: [ 'PhantomJS' ],
    singleRun: true,
    coverageReporter: {
      dir: 'coverage/',
      type: 'html'
    },
    webpack: webpack,
    webpackMiddleware: {
      noInfo: true
    }
  });
};
