const merge = require('webpack-merge');

const TARGET = process.env.npm_lifecycle_event;

process.env.BABEL_ENV = TARGET;

const common = require('./webpack-config/common');

if(TARGET === 'start' || !TARGET) {
  const conf = merge(common, require('./webpack-config/serve'));
  module.exports = conf;
}

if(TARGET === 'build' ||  TARGET === 'build:dev' || TARGET === 'stats') {
  module.exports = merge(common, require('./webpack-config/build'))
}

if(TARGET === 'analyze') {
  module.exports = merge(common, require('./webpack-config/build'), require('./webpack-config/analyze'))
}

if(TARGET === 'test' || TARGET === 'test:debug' || TARGET === 'test:watch' || TARGET === 'tdd') {
  module.exports = merge(common, require('./webpack-config/test'));
}
