import fetchMock from 'fetch-mock';
import { stringify } from 'query-string';

export const mockGET = ({ instanceUrl = 'https://na35.salesforce.com', statusCode = 200, endpoint, res, params }) => {
  const finalParams = params ? `?${stringify(params)}` : '';

  const requestUrl = `${instanceUrl}${endpoint}${finalParams}` ;

  return fetchMock.getOnce(requestUrl, {
    status: statusCode,
    body: res
  });

}

export const mockPOST = ({ instanceUrl = 'https://na35.salesforce.com', statusCode = 200, endpoint, res, params }) => {
  const requestUrl = `${instanceUrl}${endpoint}` ;

  return fetchMock.postOnce(requestUrl, {
    status: statusCode,
    body: res
  });

}

export const mockPATCH = ({ instanceUrl = 'https://na35.salesforce.com', statusCode = 200, endpoint, res, params }) => {
  const requestUrl = `${instanceUrl}${endpoint}` ;

  return fetchMock.patchOnce(requestUrl, {
    status: statusCode,
    body: res
  });
}

export const teardownMock = () => {
  return fetchMock.restore();
};
