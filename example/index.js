import * as sdal from '../src/index.js';

console.log('You can access the SDAL at window.sdal. Happy hacking.');

window.sdal = sdal;

sdal.init({
  clientId: '3MVG9uudbyLbNPZNgxQj_V2dMwtlban794Nf2MVfMBIZYwY95mhC_S0fIzfCBuq_mYqBhm1UiMv0R6mw8b4dX',
  redirectUri: 'https://localhost:8080/callback',
  remoteObject: {
    namespace: 'SObjectModel',
    models: {
      Case: {
        fields: ['Id', 'Subject']
      }
    }
  }
})
