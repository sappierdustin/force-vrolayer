import chai, { expect } from 'chai';
import { spy, stub, mock } from 'sinon';
import RemoteObject, { DEFAULT_WHERE_CRITERIA } from './remote-object';
import sinonChai from 'sinon-chai';
chai.use(sinonChai);

const _RO = new RemoteObject();

describe('remote objects', () => {
  const config = {
    remoteObject: {
      namespace: 'TestNamespace'
    }
  };

  const roWindowConfig = {
  };

  beforeEach(() => {
    _RO.init(config);

    roWindowConfig.Temp = () => {};

    window[config.remoteObject.namespace] = roWindowConfig;
  });

  describe('isActive', () => {
    it('should return true when the window var defined is available', () => {
      expect(_RO.isActive()).to.be.true;
    });

    it('should return false when the window var defined is not available', () => {
      delete window[config.remoteObject.namespace];
      expect(_RO.isActive()).to.be.false;
    });
  })

  describe('update', () => {
    it('should take the list of response ids, and instead return an object with a id field', () => {
      const mockUpdate = (criteria, ids, cb) => {
        cb(null, [ 123 ])
      };

      const tempStub = stub(roWindowConfig, 'Temp', () => ({
        update: mockUpdate
      }));

      const expected = [ 123 ];

      return _RO.update({ type: 'Temp', values: { Id: '123' } }).then(res => {
        expect(res).to.deep.equal(123);
      });
    });
  });

  describe('create', () => {
    it('should take the list of response ids, and instead return an object with a id field', () => {
      const mockUpdate = (criteria, cb) => {
        cb(null, [ 123 ])
      };

      const tempStub = stub(roWindowConfig, 'Temp', () => ({
        create: mockUpdate
      }));

      const expected = [ 123 ];

      return _RO.create({ type: 'Temp', values: { } }).then(res => {
        expect(res).to.deep.equal(123);
      });
    });
  });

  describe('identity', () => {
    it('should use the configured identity namespace to get a window var', () => {
      _RO.init({
        identity: {
          namespace: 'test'
        }
      })

      const expected = { a: 'b' };

      window['test'] = expected;

      return _RO.identity().then(res => expect(res).to.equal(expected));
    });
  });

  describe('retrieve', () => {
    it('should use the default critera when no criteria is provided', () => {
      const mockRetrieve = spy();

      const tempStub = stub(roWindowConfig, 'Temp', () => ({
        retrieve: mockRetrieve
      }));

      _RO.retrieve({ type: 'Temp' });

      expect(mockRetrieve).to.have.been.calledWith({ where: DEFAULT_WHERE_CRITERIA });
    });

    it('should return a list of objects from it\'s promise', () => {
      const mockRetrieve = (criteria, cb) => {
        cb(null, [{ _props: { Id: 1 } }, { _props: { Id: 2 } }])
      };

      const tempStub = stub(roWindowConfig, 'Temp', () => ({
        retrieve: mockRetrieve
      }));

      const expected = [{ Id: 1 }, { Id: 2 }];

      return _RO.retrieve({ type: 'Temp' }).then(res => {
        expect(res).to.deep.equal(expected);
      });
    })
  });
});
