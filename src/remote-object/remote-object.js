import AutobindClass from '../autobind-class';

export const DEFAULT_WHERE_CRITERIA = { Id: { ne : '' } };
const DEFAULT_CONFIG = {
  remoteObject: {
    namespace: 'FVRO'
  },
  identity: {
    namespace: 'identity'
  }
};

export default class RemoteObject extends AutobindClass {

  constructor() {
    super();

    this.init({ });
  }

  init(config) {
    this.config = { ...DEFAULT_CONFIG, ...config };
  }


  isActive() {
    return !!this.getNamespace();
  }

  getNamespace() {
    return window[this.config.remoteObject.namespace];
  }

  getObjectInstance(type) {
    const ObjInstance = this.getNamespace()[type];
    return new ObjInstance();
  }

  execute(type, action, ...args) {
    const entity = this.getObjectInstance(type);

    return new Promise((resolve, reject) => {
      entity[action](...args, (err, res) => {
        if(err) {
          return reject(err);
        }

        return resolve(res);
      });
    });
  }

  del({ type, ids }) {
    return this.execute(type, 'del', ids);
  }

  update({ type, values }) {
    return this.execute(type, 'update', [values.Id], values)
      .then(res => {
        if(res.length > 0) {
          return res[0]
        };

        return Promise.reject(`Received no records after update ${res}`);
      })
  }

  create({ type, values }) {
    return this.execute(type, 'create', values)
      .then(res => {
        if(res.length > 0) {
          return res[0]
        }

        return Promise.reject(`Received no records after create ${res}`);
      })
   }

   identity() {
     return new Promise((resolve, reject) => {
       const identity = window[this.config.identity.namespace];

       if(identity) {
         return resolve(identity);
       }

       return reject(`Could not find an identity object at ${this.config.identity.namespace}`);
     });
   }

   retrieve({ type, criteria }) {
     if(typeof criteria === 'undefined') {
       criteria = {};
     }

     if(criteria.offset !== 'undefined' && criteria.offset === 0) {
       delete criteria.offset;
     }

     if(typeof(criteria.where) === 'undefined') {
       criteria.where = DEFAULT_WHERE_CRITERIA;
     }

     return this.execute(type, 'retrieve', criteria)
      .then(recs => recs.map(rec => rec._props));
   }
}
