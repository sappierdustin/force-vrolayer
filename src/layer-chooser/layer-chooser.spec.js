import chai, { expect } from 'chai';
import { retrieve, init, setLayers, activeLayer, create, update, upsert, del } from './layer-chooser';
import { stub, spy } from 'sinon';
import sinonChai from 'sinon-chai';
import defaultConfig from '../defaults';

chai.use(sinonChai);

describe('layer-chooser', () => {
  let mockLayer;

  beforeEach(() => {
    mockLayer = {
      retrieve: stub().returns(Promise.resolve([])),
      create: stub().returns(Promise.resolve({})),
      update: stub().returns(Promise.resolve({})),
      del: stub().returns(Promise.resolve({})),
      isActive: _ => true,
      init: _ => _
    };

    setLayers([ mockLayer ]);

    init({
      remoteObject: {
        models: {
          Thing__c: [ 'Id', 'Name' ]
        }
      }
    });
  });

  describe('del', () => {
    it('should call the del method on the layer, chaning a single id into ids (an array containing that id)', () => {
      del({ id: '123' });

      const expected = {
        ids: ['123'],
        id: '123',
        criteria: {},
        fields: [],
        query: '',
        type: null,
        values: []
      }

      expect(mockLayer.del).to.have.been.calledWith(expected)
    });

    it('should call the del method on the layer)', () => {
      del({ ids: [ '123' ] });

      const expected = {
        ids: ['123'],
        criteria: {},
        fields: [],
        query: '',
        type: null,
        values: []
      }

      expect(mockLayer.del).to.have.been.calledWith(expected)
    });
  });

  describe('upsert', () => {
    it('should call update if an Id field value is provided', () => {
      upsert({ type: 'Thing__c', values: {'Id': '5'} });

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c',
        criteria: {},
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: { Id: '5' }
      };

      expect(mockLayer.update).to.have.been.calledWith(expected);
      expect(mockLayer.create).not.to.have.been.called;
    });

    it('should call create if no Id field value is provided', () => {
      upsert({ type: 'Thing__c', values: {'Name': 'Bob'} });

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c',
        criteria: {},
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: { Name: 'Bob' }
      };

      expect(mockLayer.create).to.have.been.calledWith(expected);
      expect(mockLayer.update).not.to.have.been.called;
    });
  })

  describe('update', () => {
    it('should call the active layer\'s update method, passing the values', () => {

      const values = { Id: '5' };

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c',
        criteria: {},
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: { Id: '5' }
      };

      return update({ type: 'Thing__c', values })
        .then(() => {
          expect(mockLayer.update).to.have.been.calledWith(expected);
        });
    });

    it('should call the active layer\'s update method, passing the values mapped if packageName is specified', () => {

      init({
        remoteObject: {
          models: {
            Thing__c: {
              fields: [ 'Id', 'Name', 'CustomField__c' ],
              packageName: 'IFF'
            }
          }
        }
      });

      const values = { CustomField__c: '5' };

      const expected = {
        query: 'SELECT Id,Name,IFF__CustomField__c FROM IFF__Thing__c',
        criteria: {},
        type: 'IFF__Thing__c',
        fields: ['Id', 'Name', 'IFF__CustomField__c'],
        values: { IFF__CustomField__c: '5' }
      };

      return update({ type: 'Thing__c', values })
        .then(() => {
          expect(mockLayer.update).to.have.been.calledWith(expected);
        });
    });

    it('should throw an error if a field that is not referenced is used', () => {

      init({
        remoteObject: {
          models: {
            Thing__c: {
              fields: [ 'Id', 'Name' ],
              packageName: 'IFF'
            }
          }
        }
      });

      const values = { CustomField__c: '5' };

      const fn = update.bind(null, { type: 'Thing__c', values });

      expect(fn).to.throw();
    });

  });

  describe('create', () => {
    it('should call the active layer\'s create method, passing the values', () => {

      const values = { Id: '5' };

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c',
        criteria: {},
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: { Id: '5' }
      };

      return create({ type: 'Thing__c', values })
        .then(() => {
          expect(mockLayer.create).to.have.been.calledWith(expected);
        });
    });

    it('should call the active layer\'s create method, passing the values mapped if packageName is specified', () => {

      init({
        remoteObject: {
          models: {
            Thing__c: {
              fields: [ 'Id', 'Name', 'CustomField__c' ],
              packageName: 'IFF'
            }
          }
        }
      });

      const values = { CustomField__c: '5' };

      const expected = {
        query: 'SELECT Id,Name,IFF__CustomField__c FROM IFF__Thing__c',
        criteria: {},
        type: 'IFF__Thing__c',
        fields: ['Id', 'Name', 'IFF__CustomField__c'],
        values: { IFF__CustomField__c: '5' }
      };

      return create({ type: 'Thing__c', values })
        .then(() => {
          expect(mockLayer.create).to.have.been.calledWith(expected);
        });
    });

    it('should throw an error if a field that is not referenced is used', () => {

      init({
        remoteObject: {
          models: {
            Thing__c: {
              fields: [ 'Id', 'Name' ],
              packageName: 'IFF'
            }
          }
        }
      });

      const values = { CustomField__c: '5' };

      const fn = create.bind(null, { type: 'Thing__c', values });

      expect(fn).to.throw();
    });
  });

  describe('init', () => {
    it('should call init for all layers, passing down the supplied config; adding the defaults', () => {
      mockLayer.init = spy();
      const config = Object.assign({}, defaultConfig, { a: 'b' });
      init(config);
      expect(mockLayer.init).to.have.been.calledWith(config);
    });
  });

  describe('activeLayer', () => {
    it('should return the first item in the layer list that is active', () => {
      mockLayer.isActive = _ => true;
      expect(activeLayer()).to.equal(mockLayer);
    });

    it('should return undefined if no layers are active', () => {
      mockLayer.isActive = _ => false;
      expect(activeLayer()).to.be.undefined;
    });
  });

  describe('retrieve', () => {
    it('should handle IN statements correct', () => {
      const criteria = {
        where: {
          Name: { in: ['a', 'b'] }
        }
      };

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c WHERE Name IN (\'a\',\'b\')',
        criteria,
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: []
      };

      return retrieve({ type: 'Thing__c', criteria })
        .then(() => {
          expect(mockLayer.retrieve).to.have.been.calledWith(expected);
        });
    });

    it('should throw an error if a field is references that isn\'t configured', () => {
      init({
        remoteObject: {
          models: {
            Thing__c: [ 'Name' ]
          }
        }
      });

      const criteria = {
        where: {
          Id: { eq: 'abc' }
        }
      };

      const fn = retrieve.bind(null, { type: 'Thing__c', criteria });

      expect(fn).to.throw();
    });

    it('should still create a query if no criteira is supplied', () => {
      const criteria = { };

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c',
        criteria,
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: []
      };

      return retrieve({ type: 'Thing__c', criteria })
        .then(() => {
          expect(mockLayer.retrieve).to.have.been.calledWith(expected);
        });
    });

    it('should properly map complex queries', () => {

      init({
        remoteObject: {
          models: {
            Thing__c: {
              fields: [ 'CustomFieldTwo__c', 'CustomField__c' ],
              packageName: 'IFF'
            }
          }
        }
      });

      const criteria = {
        where: {
          and: {
            CustomFieldTwo__c: { eq: 'abc' },
            or: {
              CustomField__c: { eq: 'abc' },
              CustomFieldTwo__c: { eq: 'def' }
            }
          },
          CustomFieldTwo__c: { ne: '123' }
        }
      };

      const expectedQuery = "SELECT IFF__CustomFieldTwo__c,IFF__CustomField__c" +
        " FROM IFF__Thing__c WHERE (IFF__CustomFieldTwo__c = 'abc' AND" +
        " (IFF__CustomField__c = 'abc' OR IFF__CustomFieldTwo__c = 'def'))" +
        " AND IFF__CustomFieldTwo__c != '123'";

      const expected = {
        query: expectedQuery,
        criteria: {
          where: {
            and: {
              IFF__CustomFieldTwo__c: { eq: 'abc' },
              or: {
                IFF__CustomField__c: { eq: 'abc' },
                IFF__CustomFieldTwo__c: { eq: 'def' }
              }
            },
            IFF__CustomFieldTwo__c: { ne: '123' }
          }
        },
        type: 'IFF__Thing__c',
        fields: ['IFF__CustomFieldTwo__c', 'IFF__CustomField__c'],
        values: []
      };

      return retrieve({ type: 'Thing__c', criteria })
        .then(() => {
          expect(mockLayer.retrieve).to.have.been.calledWith(expected);
        });
    });

    it('should call the active layer\'s retrieve method, passing down a query, and a criteria based on the supplied criteria and type', () => {
      init({
        remoteObject: {
          models: {
            Thing__c: [ 'Id', 'Name' ]
          }
        }
      });

      const criteria = {
        where: {
          Id: { eq: 'abc' }
        }
      };

      const expected = {
        query: 'SELECT Id,Name FROM Thing__c WHERE Id = \'abc\'',
        criteria,
        type: 'Thing__c',
        fields: ['Id', 'Name'],
        values: []
      };

      return retrieve({ type: 'Thing__c', criteria })
        .then(() => {
          expect(mockLayer.retrieve).to.have.been.calledWith(expected);
        });
    });

    it('should map fields and conditions to their namespaced versions when provided', () => {
      init({
        remoteObject: {
          models: {
            Thing__c: {
              fields: [ 'Id', 'Name', 'CustomField__c' ],
              packageName: 'IFF'
            }
          }
        }
      });

      const criteria = {
        where: {
          CustomField__c: { eq: 'abc' },
          Id: { eq: '123' }
        }
      };

      const expected = {
        query: 'SELECT Id,Name,IFF__CustomField__c FROM IFF__Thing__c WHERE IFF__CustomField__c = \'abc\' AND Id = \'123\'',
        criteria: {
          where: {
            IFF__CustomField__c: { eq: 'abc' },
            Id: { eq: '123' }
          }
        },
        type: 'IFF__Thing__c',
        fields: ['Id', 'Name', 'IFF__CustomField__c'],
        values: []
      };

      return retrieve({ type: 'Thing__c', criteria })
        .then(() => {
          expect(mockLayer.retrieve).to.have.been.calledWith(expected);
        });
    });
  });
});
