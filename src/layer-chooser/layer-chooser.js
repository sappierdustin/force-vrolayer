import API from '../api/api';
import RemoteObject from '../remote-object/remote-object';
import defaultSettings from '../defaults';
import { stringify } from '../criteria-stringifer/criteria-stringifier';
import mapKeys from 'lodash/mapKeys';
import invert from 'lodash/invert';
import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';



const deep = (obj, mapper) => {
  const deepMapper = v => {
    const res = (typeof(v) === 'object' ? deep(v, mapper) : v);
    return res;
  };

  if(isArray(obj)) {
    return mapper(obj.map(deepMapper));
  }

  return mapper(mapValues(obj, deepMapper));
};

let _config = {};

const _API = new API();
const _RO = new RemoteObject();

// list of data access layers to be attempted in order
let orderedLayers = [ _RO, _API ];

export const setLayers = layers => {
  orderedLayers = layers;
};

const isCustom = value => value.indexOf('__c') >= 0;

const isRelationshipField = value => value.indexOf('__r') >= 0 || value.indexOf('.') >= 0;

const modelFields = type => {
  const modelConfig = _config.remoteObject.models[type];
  return modelConfig.fields || modelConfig;
}

const finalPackagePrefix = (type) => {
  const { remoteObject } = _config;
  const { models } = remoteObject;
  return models[type].packageName ? `${models[type].packageName}__` : '';
}

const finalField = (field, type) => {
  const prefix = finalPackagePrefix(type, _config);

  if(isRelationshipField(field)) {
    throw new Error(`Cannot use relationship fields with remoteObjects: ${field}`)
  }

  if(!isCustom(field) || !prefix || field.indexOf(prefix) >= 0) {
    return field;
  }

  return prefix + field;
}

const fieldMappings = (type) => {
  const mappings = {};
  const fields = modelFields(type);

  for(var i = 0; i < fields.length; i++) {
    const field = fields[i];
    mappings[field] = finalField(field, type);
  }

  return mappings;
}

const finalFields = (type) => {
  return modelFields(type).map(field => finalField(field, type));
}

const finalTypeName = (type) => {
  const models = _config.remoteObject.models;

  /*
    If the request specifies a type AND the user has specified a packageName
    for the type. Prepend the packageName to the type. Other wise, just return
    the specified type
  */
  if(!type || !models[type]) {
    return null
  }

  const packagePrefix = finalPackagePrefix(type);

  return `${packagePrefix}${type}`
}

const finalQuery = (type, criteria) => {
  const prefix = finalPackagePrefix(type, _config);

  const allowedFields = finalFields(type);

  const finalQuery = `SELECT ${finalFields(type).join(',')} FROM ${prefix}${type} ${stringify(criteria, allowedFields)}`.trim();

  return finalQuery;
}

const finalValues = (type, values) => {
  if(!values) {
    return [];
  }

  const mappings = fieldMappings(type);

  return mapKeys(values, (value, key) => {
    if(!mappings[key]) {
      throw new Error(`Attempting to create / update using field ${key}.`)
    }
    return mappings[key]
  });
}

const finalCriteria = (type, criteria) => {
  if(!criteria) {
    return {};
  }

  const mappings = fieldMappings(type);

  return deep(criteria, x => {
    if(isArray(x)) {
      return x.map(val => mappings[val] || val);
    }

    return mapKeys(x, (val, key) => mappings[key] || key )
  });
}

/**
  Attemps to find the first active layer by calling the isActive() function.

  If no layers are found, a reject promise is returned
**/
const callActiveLayer = (fn, { type, criteria, values, ...params} = {}) => {
  const layer = activeLayer();

  if(!layer) {
    return Promise.reject({ message: 'No layers are active' });
  }

  const mappedCriteria = criteria ? finalCriteria(type, criteria) : {};

  const finalParams = {
    ...params,
    type: type ? finalTypeName(type) : null,
    fields: type ? finalFields(type) : [],
    query: type ? finalQuery(type, mappedCriteria) : '',
    values: values ? finalValues(type, values) : [],
    criteria: mappedCriteria
  }

  return layer[fn](finalParams);
};

/**
  Initialize all the layers. All configuration options are passed to all layers.
**/
export const init = (options) => {
  _config = options;
  orderedLayers.forEach(l => l.init({...defaultSettings, ...options}));
};

export const activeLayer = () => orderedLayers.find(l => l.isActive());

/**
  Prompt a user for API access.
**/
export const login = _API.login;


/**
  Clean out user API references
**/
export const logout = _API.logout;

/**
  Create a new SObject of a specified type using the specified values.

  Promise returns the Id of the created object
**/
export const create = (params) => callActiveLayer('create', params);

/**
  Updates an existing SObject of a specified type using the specified values. Only
  a single record can be updated at a time.

  Promise returns the Id of the updated object
**/
export const update = (params) => callActiveLayer('update', params);

/**
  Delete an existing SOBject of a specified type by the id provided. Only a single
  record can be deleted at a time.

  Promise returns the Id(s) of the deleted record
**/
export const del = (params) => callActiveLayer('del', { ...params, ids: params.id ? [params.id] : params.ids });


/**
  Updates, or creates, a record. An update is only preformed if an Id is provided
  as a property of values. A create is performed otherwise.

  Returns the Id of the updated, or created, record.
**/
export const upsert = (params) => callActiveLayer(params.values.Id ? 'update' : 'create', params);

/**
  Executes a SOQL query using a specificed query.

  Promise returns the Id of the updated, or created, record.
**/
export const retrieve = (params) => callActiveLayer('retrieve', params)
  .then(res => {
    const invertedMappings = invert(fieldMappings(params.type));

    return res.map(record => mapKeys(record, (value, field) => invertedMappings[field]));
  });

/**
  Executes a SOSL search using a specificed query.

  Only available through the API.

  Promise returns the results of the search.
**/
export const search = (query) => _API.search(query);


/**
  Get a list of all SObjects available to the active user.

  Only available through the API.

  Returns a List of object names.
**/
export const sobjects = () => _API.sobjects();

/**
  Get the describe information for an sObject.

  Only available through the API.

  Promise returns the describe information for the type specified.
**/
export const describe = (type) => _API.describe(type);

/**
  Perform an apexRest request

  Only available through the API.

  Promise returns the results of the apexRest request.
  Returns a rejected promise if the given requestType is not supported
**/
export const apexRest = _API.apexRest

/**
  Get the identity for the current user using the API endpoint.

  When on a VisualForce page relies on some window variable that has been
  pre-loaded with information; for example:

  window.identity: {
    user_id: '{!$User.Id}'
    username: '{!$User.Username}',
    email: '{!$User.Email}',
    first_name: '{$User.FirstName}',
    last_name: '{!$User.LastName}'
  };

  By default, checks window['identity']

  Promise returns the identity information for the current user.
**/
export const identity = () => callActiveLayer('identity');

/**
  Check if the user has API access.

  Returns boolean
**/

export const hasApiAccess = _API.isActive;

/**
 Check if the user has VRO access

 Returns boolean
**/

export const hasRoAccess = _RO.isActive;
