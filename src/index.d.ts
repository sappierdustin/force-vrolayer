/// <reference path="../typings/index.d.ts" />

declare module 'force-vrolayer' {

  interface Values {
    id?: string;
  }

  interface Criteria {
    where: any;
    orderby?: any;
    limit?: number;
    offset?: number;
  }

  interface DeleteOptions {
    type: string;
    id?: string;
    ids: Array<String>;
  }

  interface RetrieveOptions {
    type: String;
    criteria: Criteria;
  }

  interface CRUOptions {
    type: String;
    values: any;
  }

  export interface RemoteObjectConfig {
    namespace: string;
    models: any;
  }

  export interface SalesforceConfig {
    clientId: string;
    redirectUri: string;
    remoteObject: RemoteObjectConfig;
  }

  export interface LoginResult {
    status: string
  }

  export interface LoginOptions {
    scope?: string
  }

  export interface IdentityResponse {
    first_name: string;
    last_name: string;
    email: string;
    user_id: string;
    account_id: string;
    contact_id: string;
    phone: string;
    home_phone: string;
    other_phone: string;
    mobile_phone: string;
    salutation: string;
    mailing_street: string;
    mailing_city: string;
    mailing_state: string;
    mailing_postal_code: string;
    mailing_country: string;
    mailing_state_code: string;
    mailing_country_code: string;
  }

  function activeLayer(): any;
  function login(options?: LoginOptions): Promise<LoginResult>;
  function logout(): Promise<any>;
  function retrieve(options: RetrieveOptions): any;
  function create(options: CRUOptions): any;
  function update(options: CRUOptions): any;
  function del(options: CRUOptions): any;
  function upsert(options: DeleteOptions): any;
  function init(options: SalesforceConfig): void;
  function identity(): Promise<IdentityResponse>;
  function hasApiAccess(): boolean;
  function hasRoAccess(): boolean;

}
