import { spy, stub } from 'sinon';
import API from './api';
import sinonChai from 'sinon-chai';
import chai, { expect } from 'chai';
import fetchMock from 'fetch-mock';
import { mockGET, mockPOST, mockPATCH, teardownMock } from 'test-utils';

chai.use(sinonChai);

describe('API', () => {
  describe('isActive', () => {
    it('should return true when oauth and oauth.access_token are set', () => {
      const _API = new API();
      _API.oauth = { access_token: 'abc' };
      expect(_API.isActive()).to.be.true;
    });

    it('should return false when oauth is not defined', () => {
      const _API = new API();
      _API.oauth = null;
      expect(_API.isActive()).to.be.false;
    });

    it('should return false when oauth is defined, but access_token is not', () => {
      const _API = new API();
      _API.oauth = {};
      expect(_API.isActive()).to.be.false;
    });
  });

  describe('retrive', () => {
    it('should call the endpoint for the configured type using the encoded query', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const query = 'SELECT Id FROM Test WHERE Name = \'Bananas\'';

      mockGET({
        endpoint: `/services/data/v36.0/query?q=${encodeURIComponent(query)}`,
        res: { records: [ { attributes: { }, Id: 'abc' } ] }
      });

      return _API.retrieve({ query }).then(res => {
        expect(res[0].attributes).to.not.be.defined;
        expect(res[0].Id).to.equal('abc');
      });
    });
  });

  describe('search', () => {
    it('should call the endpoint for the configured type using the encoded query', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const query = 'SELECT Id FROM Test WHERE Name = \'Bananas\'';

      mockGET({
        endpoint: `/services/data/v36.0/search?q=${encodeURIComponent(query)}`,
        res: [ { attributes: { }, Id: 'abc' } ]
      });

      return _API.search(query).then(res => {
        expect(res[0].attributes).to.not.be.defined;
        expect(res[0].Id).to.equal('abc');
      });
    });
  });

  describe('create', () => {
    it('should call the endpoint and return a single record', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const objType = 'Banana';
      const params = { Some_Field: 'c' };

      const postMock = mockPOST({
        endpoint: `/services/data/v36.0/sobjects/${objType}`,
        params,
        res: { "id": 123, success: true }
      })

      return _API.create({ type: 'Banana', values: params }).then(res => {
        expect(res).to.equal(123);
        expect(JSON.parse(postMock.lastCall()[1].body)).to.deep.equal(params);
      });
    });
  });

  describe('update', () => {
    it('should call the endpoint and return the record id', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const objType = 'Banana';
      const params = { Some_Field: 'c', Id: 123 };

      const patchMock = mockPATCH({
        endpoint: `/services/data/v36.0/sobjects/${objType}/${params.Id}`,
        params,
        statusCode: 204,
        res: null
      })

      return _API.update({ type: 'Banana', values: params }).then(res => {
        expect(res).to.equal(123);
        expect(JSON.parse(patchMock.lastCall()[1].body)).to.deep.equal(params);
      });
    });
  });

  describe('describe', () => {
    it('should call the describe endpoint for the type provided', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const type = 'Banan';
      const result = { Hello: 'World' };

      mockGET({
        endpoint: `/services/data/v36.0/sobjects/${type}/describe`,
        res: result
      });

      return _API.describe(type).then(res => {
        expect(res).to.deep.equal(result);
      });
    });
  })

  describe('sobjects', () => {
    it('should call the sobjects endpoint', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const result = { sobjects: [{ a: 'b'}, { c: 'd'}] };

      mockGET({
        endpoint: `/services/data/v36.0/sobjects`,
        res: result
      });

      return _API.sobjects().then(res => {
        expect(res).to.deep.equal(result.sobjects);
      });
    })
  });

  describe('request', () => {

    it('should return an error if the refresh fails', () => {
      const _API = new API();
      _API.init({
        clientId: 'abc', clientSecret: '123'
      });

      _API.oauth = {
        refresh_token: 'zxc',
        access_token: '123'
      };

      mockGET({
        endpoint: `/failed-endpoint`,
        statusCode: 401
      });

      const mocks = mockGET({
        endpoint: '/services/oauth2/token',
        instanceUrl: 'https://login.salesforce.com',
        statusCode: 401
      });

      const expectedRefreshCall = {
        client_id: 'abc',
        client_secret: '123',
        grant_type: 'refresh_token',
        refresh_token: 'zxc'
      };

      return _API.request({ path: '/failed-endpoint' }).catch(res => {
        expect(JSON.parse(mocks.calls().matched[1][1].body)).to.deep.equal(expectedRefreshCall);
        expect(res.statusText).to.equal('Unauthorized');
        expect(res.status).to.equal(401);
        expect(_API.oauth).to.be.null;
      });
    });

    it('should return an error if no refresh_token is available', () => {
      const _API = new API();
        _API.init({
        clientId: 'abc', clientSecret: '123'
      });

      _API.oauth = {
        access_token: '123'
      };

      mockGET({
        endpoint: `/failed-endpoint`,
        res: null,
        statusCode: 401
      });

      const mocks = mockGET({
        endpoint: '/services/oauth2/token',
        instanceUrl: 'https://login.salesforce.com',
        statusCode: 401
      });

      return _API.request({ path: '/failed-endpoint' }).catch(res => {
        expect(mocks.calls('https://login.salesforce.com/services/oauth2/token')).to.have.length(0);
        expect(res.statusText).to.equal('Unauthorized');
        expect(res.status).to.equal(401);
        expect(_API.oauth).to.be.null;
      });
    });

    it('should attempt to use the refresh_token when one is available a 401 is returned, and return the correct response when succesful', () => {
      const _API = new API();
      _API.init({
        clientId: 'abc', clientSecret: '123'
      });

      _API.oauth = {
        refresh_token: 'zxc',
        access_token: '123'
      };

      mockGET({
        endpoint: `/failed-endpoint`,
        res: null,
        statusCode: 401
      });

      const expected = { some: 'thing' };

      mockGET({
        endpoint: `/failed-endpoint`,
        res: expected,
        statusCode: 200
      });

      const mocks = mockGET({
        endpoint: '/services/oauth2/token',
        instanceUrl: 'https://login.salesforce.com',
        res: {
          access_token: '456'
        }
      });

      const expectedRefreshCall = {
        client_id: 'abc',
        client_secret: '123',
        grant_type: 'refresh_token',
        refresh_token: 'zxc'
      };

      return _API.request({ path: '/failed-endpoint' }).then(res => {
        expect(JSON.parse(mocks.calls().matched[1][1].body)).to.deep.equal(expectedRefreshCall);
        expect(res).to.deep.equal(expected);
      });
    });
  });

  describe('apexRest', () => {
    it('should call the apexRest endpoint for the provided configuraiton w/ the namespace prvoided', () => {
      const _API = new API();
      _API.init({ accessToken: '123' });

      const result = { hello: 'world' };

      const postMock = mockPOST({
        endpoint: `/services/apexrest/SomeNamespace/some/endpoint`,
        res: result
      });

      const config = {
        endpoint: '/some/endpoint',
        namespace: 'SomeNamespace',
        headers: { 'X-Some-Header': "Muh Value"},
        method: 'POST',
        params: { a: 'b' }
      };

      return _API.apexRest(config).then(res => {
        expect(res).to.deep.equal(result);
        expect(JSON.parse(postMock.lastCall()[1].body)).to.deep.equal(config.params);
        expect(postMock.lastCall()[1].headers['X-Some-Header']).to.equal('Muh Value');
      });
    });
  });

  afterEach(() => {
    fetchMock.restore();
  });
});
