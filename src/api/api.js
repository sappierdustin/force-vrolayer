import { parse } from 'query-string';
import store from 'store2';
import AutobindClass from '../autobind-class';
import trim from 'lodash/trim';

const STORAGE_NAMESPACE = 'FORCE-VRO-STORAGE';
const TOKENS_KEY = 'API-TOKENS'

export default class API extends AutobindClass {
  constructor() {
    super();
    this.init({});
    this.store = store.namespace(STORAGE_NAMESPACE);
    this.oauth = this.findExistingTokens();
  }

  findExistingTokens() {
    return this.store.get(TOKENS_KEY);
  }

  saveTokens(auth) {
    this.store.set(TOKENS_KEY, auth);
    return auth;
  }

  clearTokens() {
    this.oauth = null;
    this.store.remove(TOKENS_KEY);
  }

  init(settings) {
    this.loginUrl = settings.loginUrl || 'https://login.salesforce.com';
    this.apiVersion = settings.apiVersion || 'v36.0';
    this.clientId = settings.clientId;
    this.clientSecret = settings.clientSecret;
    this.redirectUri = settings.redirectUri;
    this.accessToken = settings.accessToken;
    this.loginInterval = settings.loginInterval || 10;
    this.instanceUrl = settings.instanceUrl || 'https://na35.salesforce.com';

    if(settings.accessToken) {
      this.oauth = { access_token: settings.accessToken };
    }
  }

  isActive() {
    return !!(this.oauth && this.oauth.access_token);
  }

  login() {
    return new Promise((res, rej) => {
      const w = window.open(this.getLoginURL(), '_blank', 'location=no,clearsessioncache=yes,clearcache=yes,toolbar=no');

      const i = setInterval(() => {
        if(!w) {
          rej('User terminated login action');
        }

        const result = this.checkLoginStatus(w);

        if (! result && w.closed){
          rej('User terminated login action');
        }

        if(!!result) {
          this.oauth = result;
          this.saveTokens(this.oauth);
          w.close();
          clearInterval(i);
          return res(result);
        }
      }, this.loginInterval);
    });
  }

  getInstanceUrl() {
    let instanceUrl;

    if(this.instanceUrl) {
      instanceUrl = this.instanceUrl;
    }

    if(this.oauth && this.oauth.instance_url) {
      instanceUrl = this.oauth.instance_url;
    }

    if(instanceUrl.slice(-1) === '/') {
      instanceUrl = instanceUrl.slice(0, -1);
    }

    return instanceUrl;
  }

  checkLoginStatus(windowInstance) {
    try {
      const currentUrl = windowInstance.location.href;

      if(currentUrl != null && currentUrl.indexOf(this.redirectUri) === 0 && currentUrl.indexOf('#') > 0) {
        return parse(decodeURIComponent(currentUrl.split('#')[1]));
      }
    } catch (e) {

    }

    return false;
  }

  getLoginURL() {
    return `${this.loginUrl}/services/oauth2/authorize?display=touch&response_type=token&client_id=${this.clientId}&redirect_uri=${this.redirectUri}`;
  }

  canRefreshToken() {
    return this.clientId && this.clientSecret && this.oauth && this.oauth.refresh_token;
  }

  refreshToken() {
    return this.request({
      method: 'GET',
      instanceUrl: this.loginUrl,
      path: '/services/oauth2/token',
      retry: false,
      body: {
        client_id: this.clientId,
        client_secret: this.clientSecret,
        refresh_token: this.oauth.refresh_token,
        grant_type: 'refresh_token'
      }
    })
    .then(({ access_token, issued_at }) => {
      this.oauth.access_token = access_token;
      this.oauth.issued_at = issued_at;
      return this.oauth;
    });
  }

  handleUnauthorized(res) {
    const retryPromise = (this.canRefreshToken() ? this.refreshToken() : Promise.reject(res));

    return retryPromise.catch(res => {
      this.clearTokens()
      return Promise.reject(res);
    });
  }

  request(config) {
    const {
      instanceUrl = this.getInstanceUrl(),
      method = 'GET', retry = true,
      path, headers, body
    } = config;

    if(!this.oauth || !this.oauth.access_token) {
      return Promise.reject('User is not authorized');
    }

    return fetch(`${instanceUrl}${path}`, {
      method,
      headers: {
        'Authorization': `Bearer ${this.oauth.access_token}`,
        ...headers
      },
      body: body ? JSON.stringify(body) : null
    })
    .then(res => {
      if(retry && res.status === 401) {
        return this.handleUnauthorized(res).then(() => this.request(config));
      }

      if (res.status >= 400) {
        return Promise.reject(res);
      }

      if(res.status === 204) {
          return null;
      }

      return res.json();
    })
    .catch(res => {
      // if the request hard failed try to login again
      if(res instanceof TypeError && res.message === 'Failed to fetch') {
        return this.handleUnauthorized();
      }

      return Promise.reject(res);
    });
  }

  del({ type, ids }) {
    return this.request({
      method: 'DELETE',
      path: `/services/data/${this.apiVersion}/sobjects/${type}/${ids}`
    });
  }

  sobjects() {
    return this.request({
      path: `/services/data/${this.apiVersion}/sobjects`
    }).then(res => res.sobjects);
  }

  update({ type, values }) {
    const id = values.Id;

    delete values.Id;
    delete values.attributes;

    return this.request({
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      path: `/services/data/${this.apiVersion}/sobjects/${type}/${id}`,
      body: values
    }).then(() => {
      return id;
    });
  }

  create({ type, values }) {
    return this.request({
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      path: `/services/data/${this.apiVersion}/sobjects/${type}`,
      body: values
    })
    .then(res => {
      if(!res || !res.id) {
        return Promise.reject(`create did not produce an object with an id: ${res}`)
      }
      return res.id;
    });
  }

  describe(type) {
    return this.request({
      path: `/services/data/${this.apiVersion}/sobjects/${type}/describe`
    });
  }

  logout() {
    this.oauth = null;
  }

  retrieve({ query }) {
    return this.request({
      path: `/services/data/${this.apiVersion}/query?q=${encodeURIComponent(query)}`
    })
    .then(res => res.records.map(r => {
      delete r.attributes;
      return r;
    }));
  }

  search(query) {
    return this.request({
      path: `/services/data/${this.apiVersion}/search?q=${encodeURIComponent(query)}`
    })
    .then(res => res.map(r => {
      delete r.attributes;
      return r;
    }));
  }

  apexRest({ method, endpoint, params, namespace, headers }) {
    const finalNamespace = namespace ? `${namespace.trim('/')}/` : '';

    return this.request({
      method,
      path: `/services/apexrest/${trim(finalNamespace)}${trim(endpoint, '/')}`,
      headers,
      body: params
    })
  }
}
