const VALID_CRITERIA_FIELDS = [
  'orderby', 'where', 'limit', 'offset'
];

const OFFSET = 'OFFSET';
const LIMIT = 'LIMIT';

const SYMBOLS = {
  EQUALITY: {
    eq: '=', ne: '!=', lt: '<',
    lte: '<=', gt: '>', gte: '>=',
    like: 'LIKE'
  },
  CONTAINS: {
    in: 'IN', nin: 'NOT IN'
  },
  LOGICAL: {
    and: 'AND', or: 'OR'
  }
};

class CriteriaStringifier {
  constructor(fields = []) {
    this.fields = fields;
  }

  stringify(criteria) {
    for(const prop in criteria) {
      if(VALID_CRITERIA_FIELDS.indexOf(prop) === -1) {
        throw new Error(`Unrecognized criteria field ${prop}. Valid fields are ${VALID_CRITERIA_FIELDS.join(', ')}`)
      }
    }

    const where = this._where(criteria.where);
    const orderby = this._orderby(criteria.orderby);
    const limit = this._limit(criteria.limit);
    const offset = this._offset(criteria.offset);

    return `${where} ${orderby} ${limit} ${offset}`.trim();
  }

  _isEqualitySymbol(sym) {
    return SYMBOLS.EQUALITY[sym];
  }

  _isContainsSymbol(sym) {
    return SYMBOLS.CONTAINS[sym];
  }

  _isLogicalSymbol(sym) {
    return SYMBOLS.LOGICAL[sym];
  }

  _value(val) {
    if(typeof(val) === 'string') {
      return `'${val}'`
    }

    return val;
  }

  _clause({statement, joinWith = 'AND'}) {

    return Object.keys(statement).map((key) => {
      const val = statement[key];
      let sym = null;

      // equality symbols are symbols defined in SYMBOLS.EQUALITY
      // these take a FIELD and compare them to a VALUE
      if (sym = this._isEqualitySymbol(key)) {

        return `${sym} ${this._value(val)}`;
      }
      else if (sym = this._isContainsSymbol(key)) {

        const joined = val.map(this._value).join(',');
        return `${sym} (${joined})`;

      }
      else if (sym = this._isLogicalSymbol(key)) {

        return `(${this._clause({ statement: val, joinWith: sym })})`;
      }
      else {
        this._checkField(key);
        return `${key} ${this._clause({ statement: val })}`;
      }
    }).join(` ${joinWith} `);
  }

  _simpleClause(term, value) {
    if(typeof(value) === 'undefined' || value === null) {
      return '';
    }

    return `${term} ${value}`;
  }

  _where(where) {
    if(where == null || Object.keys(where).length === 0) {
      return '';
    }

    return `WHERE ${this._clause({ statement: where })}`;
  }

  _orderby(orderBy) {
    if(!orderBy || orderBy.length === 0) {
      return '';
    }

    orderBy.forEach(clause => Object.keys(clause).forEach(this._checkField.bind(this)));

    return `ORDER BY ${orderBy.map(item => {
      const key = Object.keys(item)[0];
      const value = item[key];

      return `${key} ${value}`;
    }).join(',')}`;
  }

  _limit(lim) {
    return this._simpleClause(LIMIT, lim);
  }

  _offset(ofs) {
    return this._simpleClause(OFFSET, ofs);
  }

  _checkField(field) {
    if(this.fields.indexOf(field) === -1) {
      throw new Error(`Unknown field ${field}. Known fields: ${this.fields.join(', ') }`);
    }
  }
}

export const stringify = (query = {}, fields = []) => {
  const cs = new CriteriaStringifier(fields);
  return cs.stringify(query);
};
