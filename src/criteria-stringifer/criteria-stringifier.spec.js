import { expect } from 'chai';
import { orderBy, clause, where, limit, offset, stringify } from './criteria-stringifier.js';


describe('remoteObjectsDSL', () => {

  describe('stringify', () => {
    it('should work with in statements', () => {
      const input = {
        where: { Id: { in: ['aaaa', 'bbbb'] } }
      };

      const expected = 'WHERE Id IN (\'aaaa\',\'bbbb\')';

      expect(stringify(input, ['Id'])).to.equal(expected);
    });

    it('should take an object with, optionally, offset, limit, whwere and orderBy parameters, and then format it', () => {
      const input = {
        where: {
          FirstName: {
            eq: 'John'
          }
        },
        orderby: [{FirstName: 'ASC'}],
        limit: 10,
        offset: 100
      };
      const expected = 'WHERE FirstName = \'John\' ORDER BY FirstName ASC LIMIT 10 OFFSET 100';

      expect(stringify(input, [ 'FirstName' ])).to.equal(expected);
    });

    it('should throw an error when a field that isn\'t configured is referenced in the where calsue', () => {
      const input = {
        where: {
          LastName: {
            eq: 'John'
          }
        },
        orderby: [{ FirstName: 'ASC' }],
        limit: 10,
        offset: 100
      };
      const expected = 'WHERE FirstName = \'John\' ORDER BY FirstName ASC LIMIT 10 OFFSET 100';

      const fn = stringify.bind(null, input, [ 'FirstName' ]);

      expect(fn).to.throw();
    });

    it('should handle complex queries', () => {
      const input = {
        where: {
          and: {
            or: {
              and: {
                NumberOfEmployees: { lt: 1000 },
                and: {
                  NumberOfEmployees: { gt: 10 },
                }
              },
              Name: { like: '%Oil%' }
            },
            AnnualRevenue: { gt: 0}
          }
        },
        orderby: [{Rating: 'DESC NULLS LAST'}, {AnnualRevenue: 'DESC'}, {NumberOfEmployees: 'ASC'}],
        limit: 1000,
        offset: 0
      };

      const expected = 'WHERE ' +
      '(((NumberOfEmployees < 1000 AND (NumberOfEmployees > 10)) OR Name LIKE \'%Oil%\') ' +
      'AND ' +
      'AnnualRevenue > 0) ' +
      'ORDER BY Rating DESC NULLS LAST,AnnualRevenue DESC,NumberOfEmployees ASC ' +
      'LIMIT 1000 ' +
      'OFFSET 0';

      expect(stringify(input, ['NumberOfEmployees', 'Name', 'Rating', 'AnnualRevenue' ])).to.equal(expected);
    });

    it('should throw an error when an invalid criteria field is provided', () => {
      const input = {
        orderBy: ''
      };

      expect(stringify.bind(this, input, [])).to.throw();
    });

    it('should format orderby statements', () => {
      const input = {
        orderby: [{FirstName: 'ASC DESC NULL'}]
      };

      const expected = 'ORDER BY FirstName ASC DESC NULL';

      expect(stringify(input, [ 'FirstName' ])).to.equal(expected);
    });

    it('should throw an error if the order by statement reference a field that isn\'t configured', () => {
      const input = {
        orderby: [{LastName: 'ASC DESC NULL'}]
      };

      const expected = 'ORDER BY FirstName ASC DESC NULL';

      const fn = stringify.bind(null, input, [ 'FirstName' ]);

      expect(fn).to.throw();
    });

    it('should format limit statements', () => {
      const input = {
        limit: 5
      };

      const expected = 'LIMIT 5';

      expect(stringify(input, [])).to.equal(expected);
    });
  });
});
