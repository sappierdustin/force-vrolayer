import autobind from 'class-autobind';

class Autobind {
  constructor() {
    autobind(this);
  }
}

export default Autobind;
