const PATHS = require('./paths');

const isDebug = process.argv.some((arg) => {
  return arg === '--debug';
});

module.exports = {
  devtool: 'inline-source-map',
  module: {
    rules: isDebug ? [] : [{
      test: /.js$/,
      include: [ PATHS.src ],
      loader: 'istanbul-instrumenter-loader',
      exclude: /(node_modules|\.spec\.(js|jsx)$)/,
      enforce: 'post'
    }]
  }
};
