const path = require('path');

// get the the root directory name
const dir = path.dirname(__dirname);

module.exports = {
  src: path.join(dir, 'src'),
  example: path.join(dir, 'example'),
  node_modules: path.join(dir, 'node_modules'),
  test: path.join(dir, 'test'),
  exampleBuild: path.join(dir, 'example/dist')
};
