const webpack = require('webpack');
const PATHS = require('./paths');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    example: PATHS.example
  },
  output: {
    path: PATHS.exampleBuild,
    filename: '[name].js'
  },
  devServer: {
    historyApiFallback: true, 
    stats: 'errors-only',
    host: process.env.HOST,
    port: process.env.PORT,
    https: true
  },
  devtool: '#source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'node_modules/html-webpack-template/index.ejs',
      title: 'Force Datalayer Playground',
      inject: false
    })
  ]
};
