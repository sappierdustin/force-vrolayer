'use strict';

const webpack = require('webpack');
const pkg = require('../package.json');
const path = require('path');
const PATHS = require('./paths');

const TARGET = process.env.BABEL_ENV;
const CleanPlugin = require('clean-webpack-plugin');


module.exports = {
  output: {
    path: PATHS.dist,
    libraryTarget: 'commonjs',
    filename: 'index.js'
  },
  plugins: [
    new CleanPlugin([PATHS.dist], { root: process.cwd() }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      beautify: false,
      mangle: {
        screw_ie8: true,
        keep_fnames: true
      },
      compress: {
        screw_ie8: true,
        warnings: false
      },
      comments: false
    })
  ]
}
