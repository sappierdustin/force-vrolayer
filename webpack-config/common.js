const PATHS = require('./paths');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    src: PATHS.src
  },
  resolve: {
    extensions: ['.js']
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.json$/,
        use: "json-loader"
      },
      {
        test: /\.js?$/,
        use: [ 'babel-loader' ],
        include: [ PATHS.example, PATHS.src, PATHS.test ]
      }
    ]
  },
  resolve: {
    modules: [ PATHS.src, PATHS.node_modules, PATHS.test ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
  ]
}
