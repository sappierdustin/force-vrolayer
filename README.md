# Salesforce Data Access Layer

The Salesforce Data Access Layer (DAL) is a simple set of classes to intelligently
switch between Visualforce Remote Objects and the Salesforce REST API based on
availability and configurations.

This library was built to allow developers to write code on their own machines,
using the Salesforce REST API, but then seamlessly switch over to Visualforce
Remote Objects when being hosted in Salesforce. This helps reduce overall limits usage. If a feature is unavailable using the Remote Objects library.

All responses are normalized to the Remote Object returns when either the REST API
or Remote Objects could be used. Otherwise, the raw API response is returned.

The Salesforce DAL is (currently) capable of accessing the following information;
all functions return a promise.

##### Available through API & Remote Objects
- `create(options)` create a new SObject
```javascript
  create({
    type: 'Case',
    values: { Name: 'My New Case' }
  })
```
- `update(options)` updates an existing SObject; requires that the values in
contain an Id property
```javascript
  update({
    type: 'Case',
    values: { Name: 'My New Case', Id: 'ABC123' }
  })
```

- `del(options)` deletes a record by Id
```javascript
  del({
    type: 'Case',
    Id: 'ABC123'
  })
```

- `upsert(options)` creates, or updates, a record. If Id is provided, an update
is executed; otherwise a create is executed.
```javascript
  /* Triggers an update */

  upsert({
    type: 'Case',
    values: { Name: 'ABC123', Id: 'ABC' }
  })
```
```javascript
  /* Triggers a create */

  upsert({
    type: 'Case',
    values: { Name: 'ABC123' }
  })
```
- `retrieve(options)` query for objects based on some criteria; criteria structued
defined here: http://sforce.co/1U8JMx9. Objects are returned in the form:
`[ { Id: '1', ... }, { Id: '2', ... } ]`. A maximum of 100 records will be returned,
to get additional objects, set the OFFSET parameter.
```javascript
  retrieve({
    type: 'Case',
    criteria: {
      where: { Name: { eq: 'ABC123' } }
    }
  })
```

##### Available through API only
- `sobjects()` returns a list of all SObject available to the active user. If an API
token is not set, the promise will reject
```javascript
  sobjects()
```

- `describe(name)` returns the describe results for the selected object. If an API
token is not set, the promise will reject.
```javascript
  describe('Case')
```

- `search(options)` returns the results of a SOSL search with the given query. If an API
token is not set, the promise will reject
```javascript
  sobjects()
```

##### Configuration
- `init(options)` initializes both the api and ro layers. the accepted parameters
are:
  - **clientId**: mandatory for using the api layer; set to the Client ID of API App
  - **redirectUri**: mandatory for using the api layer; set to a URL local to your App
  - **remoteObject**: mandatory for all layers, object containg sObject specific configurations
    - **namespace**: the apex:remoteObject name param; used to access the remote objects
    entity inside of Salesforce
    - **model**: map of models to access in Salesforce; sobject types should be the
    key; value is a list of field names

- `login()` start or update an existing API session. This will trigger a popup.
Not necessary when using just remote objects.

- `logout()` stop an API session. A call to logout will be required to make more calls

##### Example

```javascript
  {
    clientId: 'MY_APP_ID',
    redirectUri: 'https://localhost:8080',
    remoteObject: {
      namespace: 'SObjectModel',
      models: {
        Case: ['Id', 'Name'],
        Account: ['Id', 'Status', 'SomeCustomField__c', 'SomeNamespace__SpecialField__c'],
        SomeNamespace__CustomObject__c: ['Id']
      }
    }
  }
```

#### Dependencies

This project depends upon the existing of the fetch API and a Promise implementation.
If you need to support non-modern browsers, you can use these polyfills to
handle these dependencies:

**fetch**: https://github.com/matthew-andrews/isomorphic-fetch
**Promise**: https://github.com/petkaantonov/bluebird

Additionally, this code is written in ES7 notation. You should import it and pass
it through a babel processor.
